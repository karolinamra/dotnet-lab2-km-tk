﻿/*********************
 * Środa 18:55-20:30 *
 * Tadeusz Kwak      *
 * Karolina Mraczek  *
 * *******************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    class StanGry
    {
        int size = 10;
        int[,] tablicaStanow = new int[10, 10];

        public StanGry()
        {
            Random rnd = new Random();
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    tablicaStanow[j, i] = rnd.Next(2);
                }
            }
        }

        public void aktualizujPole()
        {
            int[,] tablicaPom = new int[size, size];
            int count = 0;

            for (int i = 1; i < size - 1; i++)
            {
                for (int j = 1; j < size - 1; j++)
                {
                    count = 0;
                    if (tablicaStanow[i - 1, j - 1] == 1) count++;
                    if (tablicaStanow[i, j - 1] == 1) count++;
                    if (tablicaStanow[i, j + 1] == 1) count++;
                    if (tablicaStanow[i - 1, j] == 1) count++;
                    if (tablicaStanow[i + 1, j] == 1) count++;
                    if (tablicaStanow[i + 1, j - 1] == 1) count++;
                    if (tablicaStanow[i - 1, j + 1] == 1) count++;
                    if (tablicaStanow[i + 1, j + 1] == 1) count++;
                    if (count == 3 || (count == 2 && tablicaStanow[i, j] == 1)) tablicaPom[i, j] = 1;
                    else tablicaPom[i, j] = 0;
                }
            }
            for (int i = 1; i < size - 1; i++)
            {
                count = 0;
                if (tablicaStanow[0, i - 1] == 1) count++;
                if (tablicaStanow[0, i + 1] == 1) count++;
                if (tablicaStanow[1, i - 1] == 1) count++;
                if (tablicaStanow[1, i] == 1) count++;
                if (tablicaStanow[1, i + 1] == 1) count++;
                if (count == 3 || (count == 2 && tablicaStanow[0, i] == 1)) tablicaPom[0, i] = 1;
                else tablicaPom[0, i] = 0;
            }
            for (int i = 1; i < size - 1; i++)
            {
                count = 0;
                if (tablicaStanow[i - 1, 0] == 1) count++;
                if (tablicaStanow[i + 1, 0] == 1) count++;
                if (tablicaStanow[i - 1, 1] == 1) count++;
                if (tablicaStanow[i, 1] == 1) count++;
                if (tablicaStanow[i + 1, 1] == 1) count++;
                if (count == 3 || (count == 2 && tablicaStanow[i, 0] == 1)) tablicaPom[i, 0] = 1;
                else tablicaPom[i, 0] = 0;
            }
            for (int i = 1; i < size - 1; i++)
            {
                count = 0;
                if (tablicaStanow[size - 1, i - 1] == 1) count++;
                if (tablicaStanow[size - 1, i + 1] == 1) count++;
                if (tablicaStanow[size - 2, i - 1] == 1) count++;
                if (tablicaStanow[size - 2, i] == 1) count++;
                if (tablicaStanow[size - 2, i + 1] == 1) count++;
                if (count == 3 || (count == 2 && tablicaStanow[size - 1, i] == 1)) tablicaPom[size - 1, i] = 1;
                else tablicaPom[size - 1, i] = 0;
            }
            for (int i = 1; i < size - 1; i++)
            {
                count = 0;
                if (tablicaStanow[i - 1, size - 1] == 1) count++;
                if (tablicaStanow[i + 1, size - 1] == 1) count++;
                if (tablicaStanow[i - 1, size - 2] == 1) count++;
                if (tablicaStanow[i, size - 2] == 1) count++;
                if (tablicaStanow[i + 1, size - 2] == 1) count++;
                if (count == 3 || (count == 2 && tablicaStanow[i, size - 1] == 1)) tablicaPom[i, size - 1] = 1;
                else tablicaPom[i, size - 1] = 0;
            }

            count = 0;
            if (tablicaStanow[1, 0] == 1) count++;
            if (tablicaStanow[1, 1] == 1) count++;
            if (tablicaStanow[0, 1] == 1) count++;
            if (count == 3 || (count == 2 && tablicaStanow[0, 0] == 1)) tablicaPom[0, 0] = 1;
            else tablicaPom[0, 0] = 0;
            count = 0;
            if (tablicaStanow[0, size - 2] == 1) count++;
            if (tablicaStanow[1, size - 2] == 1) count++;
            if (tablicaStanow[1, size - 1] == 1) count++;
            if (count == 3 || (count == 2 && tablicaStanow[0, size - 1] == 1)) tablicaPom[0, size - 1] = 1;
            else tablicaPom[0, size - 1] = 0;
            count = 0;
            if (tablicaStanow[size - 1, 1] == 1) count++;
            if (tablicaStanow[size - 2, 1] == 1) count++;
            if (tablicaStanow[size - 2, 0] == 1) count++;
            if (count == 3 || (count == 2 && tablicaStanow[size - 1, 0] == 1)) tablicaPom[size - 1, 0] = 1;
            else tablicaPom[size - 1, 0] = 0;
            count = 0;
            if (tablicaStanow[size - 1, size - 2] == 1) count++;
            if (tablicaStanow[size - 2, size - 1] == 1) count++;
            if (tablicaStanow[size - 2, size - 2] == 1) count++;
            if (count == 3 || (count == 2 && tablicaStanow[size - 1, size - 1] == 1)) tablicaPom[size - 1, size - 1] = 1;
            else tablicaPom[size - 1, size - 1] = 0;


            tablicaStanow = tablicaPom;

        }

        public void wyswietl()
        {
            for (int i = 0; i < size; i++)
            {

                for (int j = 0; j < size; j++)
                {


                    Console.Write(tablicaStanow[j, i]);
                }
                Console.Write('\n');
            }
        }
    }
}